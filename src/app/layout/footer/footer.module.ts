import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FooterComponent } from 'src/app/layout/footer/footer.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
    // [...]
    declarations: [
        FooterComponent,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        FooterComponent,
    ],
    providers: []
})
export class FooterModule { }
