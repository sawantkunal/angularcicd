import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeaderComponent } from 'src/app/layout/header/header.component';
import { NgxLoadingModule } from 'ngx-loading';
import { RouterModule } from '@angular/router';
import { LoginModule } from 'src/app/auth/login/login.module';

@NgModule({
    // [...]
    declarations: [
        HeaderComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        LoginModule
    ],
    exports: [
        HeaderComponent,
    ],
    providers: []
})
export class HeaderModule { }
