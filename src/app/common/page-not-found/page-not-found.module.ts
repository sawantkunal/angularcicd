import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from './page-not-found.component';
import { PageNotFoundRoutingModule } from './page-not-found.routing.module';
import { HeaderModule } from 'src/app/layout/header/header.module';
import { FooterModule } from 'src/app/layout/footer/footer.module';

@NgModule({
    // [...]
    declarations: [PageNotFoundComponent],
    imports: [
        CommonModule,
        PageNotFoundRoutingModule,
        HeaderModule,
        FooterModule
    ],
    exports: [
        PageNotFoundComponent
    ],
    providers: []
})
export class PageNotFoundModule { }
