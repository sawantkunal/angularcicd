import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  user: any;
  userFlag = false;
  constructor() { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('auth'));
    if (this.user.data.token) {
      this.userFlag = true;
    } else {
      this.userFlag = false;
    }
  }

}
