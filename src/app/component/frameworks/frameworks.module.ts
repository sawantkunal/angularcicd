import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FrameworksRoutingModule } from './frameworks.routing.module';
import { FrameworksComponent } from './frameworks.component';
import { FrameworksService } from './frameworks.service';
import { NgxLoadingModule } from 'ngx-loading';
import { ToDoModule } from '../../shared/to-do-div/to-do-div.module';
import { FooterModule } from '../../layout/footer/footer.module';
import { HeaderModule } from '../../layout/header/header.module';
import { RouterModule } from '@angular/router';

@NgModule({
    // [...]
    declarations: [
        FrameworksComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        FrameworksRoutingModule,
        HeaderModule,
        FooterModule,
        ToDoModule,
        NgxLoadingModule.forRoot({})
    ],
    exports: [
        FrameworksComponent,
    ],
    providers: [FrameworksService]
})
export class FrameworksModule { }
