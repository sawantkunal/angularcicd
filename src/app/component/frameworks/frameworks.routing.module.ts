import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrameworksComponent } from './frameworks.component';
import { LoginAuthService } from 'src/app/services/authServices/login-auth.service';

const routes: Routes = [
    {
        path: 'framework',
        canActivate: [LoginAuthService],
        component: FrameworksComponent
    },
    {
        path: '**', redirectTo: '/page-not-found'
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FrameworksRoutingModule { }
