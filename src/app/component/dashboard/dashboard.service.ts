import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getToDo() {
    return this.http.get('control/getAllframeworkmaster')
      .pipe(map(res => {
        return res;
      }));
  }
}
