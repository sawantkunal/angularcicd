import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { LoginAuthService } from 'src/app/services/authServices/login-auth.service';

const routes: Routes = [
    {
        path: 'dashboard',
        canActivate: [LoginAuthService],
        component: DashboardComponent
    },
    // {
    //     path: '**', redirectTo: '/page-not-found'
    // },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
