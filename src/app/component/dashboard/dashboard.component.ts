import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { error } from 'util';
// import { ngxLoadingAnimationTypes } from 'ngx-loading';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  toDo: any;
  // public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  loading = false;
  @ViewChild('customLoadingTemplate')
  // public customLoadingTemplate: TemplateRef<any>;
  public loadingTemplate: TemplateRef<any>;
  //  @ViewChild('container', { read: ViewContainerRef }) _vcr;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>;
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.loading = true;
    setTimeout(() => {
      this.dashboardService.getToDo()
        .subscribe(res => {
          this.toDo = res;
          this.loading = false;
        }, err => {
          this.loading = false;
        });

    }, 1000);
  }


}
