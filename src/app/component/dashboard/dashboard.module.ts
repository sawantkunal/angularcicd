import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DashboardRoutingModule } from './dashboard.routing.module';
import { DashboardComponent } from './dashboard.component';
import { HeaderComponent } from 'src/app/layout/header/header.component';
import { FooterComponent } from 'src/app/layout/footer/footer.component';
import { DashboardService } from './dashboard.service';
import { NgxLoadingModule } from 'ngx-loading';
import { HeaderModule } from '../../layout/header/header.module';
import { FooterModule } from '../../layout/footer/footer.module';
import { ToDoModule } from '../../shared/to-do-div/to-do-div.module';
import { RouterModule } from '@angular/router';

@NgModule({
    // [...]
    declarations: [
        DashboardComponent,
        // HeaderComponent,
        // FooterComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        DashboardRoutingModule,
        HeaderModule,
        FooterModule,
        ToDoModule,
        NgxLoadingModule.forRoot({})
    ],
    exports: [
        DashboardComponent,
        // HeaderComponent,
        // FooterComponent,
    ],
    providers: [DashboardService]
})
export class DashboardModule { }
