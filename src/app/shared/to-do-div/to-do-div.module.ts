import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ToDoComponent } from './to-do-div.component';
import { NgxLoadingModule } from 'ngx-loading';


@NgModule({
    // [...]
    declarations: [
        ToDoComponent,
    ],
    imports: [
        CommonModule,
        NgxLoadingModule.forRoot({})
    ],
    exports: [
        ToDoComponent,
    ],
    providers: []
})
export class ToDoModule { }
