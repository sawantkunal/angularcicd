import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DashboardService } from '../../component/dashboard/dashboard.service';

@Component({
  selector: 'to-do-div',
  templateUrl: './to-do-div.component.html',
  styleUrls: ['./to-do-div.component.css']
})
export class ToDoComponent implements OnInit {
  toDo: any;
  // public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  loading = false;
  @ViewChild('customLoadingTemplate')
  // public customLoadingTemplate: TemplateRef<any>;
  public loadingTemplate: TemplateRef<any>;
  //  @ViewChild('container', { read: ViewContainerRef }) _vcr;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>;
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.loading = true;
    setTimeout(() => {
      this.dashboardService.getToDo()
        .subscribe(res => {
          this.toDo = res;
          this.loading = false;
        }, err => {
          this.loading = false;
        });

    }, 1000);
  }

}
