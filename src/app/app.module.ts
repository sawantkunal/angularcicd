import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginModule } from './auth/login/login.module';
import { PageNotFoundModule } from './common/page-not-found/page-not-found.module';
import { DashboardModule } from './component/dashboard/dashboard.module';
import { FrameworksModule } from './component/frameworks/frameworks.module';
import { ToDoModule } from './shared/to-do-div/to-do-div.module';
import { HeaderModule } from './layout/header/header.module';
import { FooterModule } from './layout/footer/footer.module';
import { RouterModule } from '@angular/router';
import { JwtInterceptor, ErrorInterceptor, APIInterceptor } from './helpers';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    // NgxLoadingModule.forRoot({}),
    LoginModule,
    DashboardModule,
    PageNotFoundModule,
    FrameworksModule,
    ToDoModule,
    HeaderModule,
    FooterModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: APIInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})


export class AppModule { }
