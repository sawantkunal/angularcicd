import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private loginService: LoginService) {
    if (this.loginService.currentUserValue) {
      this.router.navigate(['/dashboard']);
    }
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [''],
      password: [''],
    });
  }


  submitLoginForm() {
    if (this.loginForm.valid) {
      this.loading = true;
      this.loginService.login(this.loginForm.value)
        .subscribe((res: any) => {

          this.router.navigate(['/dashboard']);
          this.loading = false;
        });
    }
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('auth');
    // this.currentUserSubject.next(null);
  }

}




