import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { LoginAuthService } from 'src/app/services/authServices/login-auth.service';

@NgModule({
    // [...]
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        FormsModule,
        LoginRoutingModule,
        ReactiveFormsModule,
        NgxLoadingModule.forRoot({})
    ],
    exports: [
        LoginComponent
    ],
    providers: [LoginService, LoginAuthService]
})
export class LoginModule { }
